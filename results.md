# Результаты
| Модель        | precision, recall, f1, pk_score, wd_score, ghd_score
| ------------- |:-------------:| 
| линейная | (0.25, 0.16, 0.18, 0.48, 0.6, 6.73) | 
| gru(10ep) | (0.04, 0.06, 0.04, 0.54, 0.54, 6.73) |
| gru(100ep) |   (0.08, 0.1, 0.08, 0.52, 0.53, 6.55) |
| rnn(10ep) | (0.01, 0.02, 0.01, 0.56, 0.56, 7.09) |
| rnn(100ep)| (0.02, 0.03, 0.02, 0.55, 0.55, 6.95) |
| hlstm_ep_85 | (0.64, 0.68, 0.64, 0.21, 0.24, 2.69) |


