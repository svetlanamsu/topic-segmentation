import pandas as pd
from utils.logger_supports import stdout_handler
from torch.functional import F

import logging
logger = logging.Logger('save result', level=logging.DEBUG)
logger.addHandler(stdout_handler())


def save_results(model, data_loader, device, file_name):
    logger.info(f'Start saving result for {model} in {file_name}.')
    out = pd.DataFrame()
    for batch in data_loader:
        input_embeds = batch["inputs"].to(device)
        labels = batch["labels"]
        prediction = F.softmax(model(input_embeds), dim=1).argmax(dim=1).to('cpu')
        temp = pd.DataFrame()
        temp['true_labels'] = labels
        temp['prediction'] = prediction
        out = pd.concat([out, temp], axis=0)
    out.to_csv(f'../data/{file_name}.csv', index=False)
    logger.info(f'Results saving in file {file_name}.')

