import torch
import pandas as pd
from sklearn.model_selection import train_test_split
from navec import Navec

from utils.trainer import training
from utils.saver import save_results
from utils.logger_supports import stdout_handler

import logging
logger = logging.Logger('check_model', level=logging.DEBUG)
logger.addHandler(stdout_handler())


def train_and_test(model, device, test_ds_file_name, ds_file_name, navec, CollatorClass, dataloader_creator, DatasetClass,
                   sampler, criterion, num_epochs, max_grad_norm, saving=True, file_name_for_saving='test_result',
                   TextSampler=None):
    logger.info('Start train and test.')
    torch.random.manual_seed(42)
    torch.cuda.random.manual_seed_all(42)
    logger.info(f'Load dataset from {test_ds_file_name}.')
    test_dataset = pd.read_csv(test_ds_file_name)

    logger.info(f'Load dataset from {ds_file_name}.')
    dataset = pd.read_csv(ds_file_name)

    train_data, valid_data = train_test_split(dataset, test_size=.2, random_state=100500)

    embed_size = navec.as_gensim.vector_size
    my_collator = CollatorClass(emb_size=embed_size, pad_token=navec.vocab['<pad>'])

    train_loader = dataloader_creator(data=train_data, DatasetClass=DatasetClass, Sampler=sampler,
                                      TextSampler=TextSampler, navec=navec, collate_fn=my_collator)
    valid_loader = dataloader_creator(data=valid_data, DatasetClass=DatasetClass, Sampler=sampler,
                                      TextSampler=TextSampler, navec=navec, collate_fn=my_collator)

    # optimizer = torch.optim.Adadelta(model.parameters())
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

    training(model=model,
             criterion=criterion,
             optimizer=optimizer,
             num_epochs=num_epochs,
             train_loader=train_loader,
             valid_loader=valid_loader,
             device=device,
             max_grad_norm=max_grad_norm
             )

    # save test results in file
    if saving:
        test_loader = dataloader_creator(data=test_dataset, DatasetClass=DatasetClass, Sampler=sampler,
                                         TextSampler=TextSampler, navec=navec, collate_fn=my_collator)
        save_results(model=model, data_loader=test_loader, device=device,
                     file_name=f'{file_name_for_saving}_ep_{num_epochs}')
        torch.save(model.state_dict(), f'../data/models/{file_name_for_saving}_state_ep_{num_epochs}.pt')


