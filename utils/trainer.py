from tqdm.notebook import tqdm
import torch

from utils.logger_supports import stdout_handler

import logging
logger = logging.Logger('trainer', level=logging.DEBUG)
logger.addHandler(stdout_handler())


def training(model, criterion, optimizer, num_epochs, train_loader, valid_loader, device='cpu', max_grad_norm=None):
    logger.info(f'Start training. Optimizer = {optimizer}, num_epochs={num_epochs}, device={device}, model={model}')
    for e in range(num_epochs):
        model.train()
        num_iter = 0
        pbar = tqdm(train_loader)
        for batch in pbar:
            input_embeds = batch["inputs"].to(device)
            labels = batch["labels"].to(device)
            optimizer.zero_grad()
            prediction = model(input_embeds)
            loss = criterion(prediction, labels)
            loss.backward()
            pbar.update(labels.size(0))
            if max_grad_norm is not None:
                torch.nn.utils.clip_grad_norm_(model.parameters(), max_grad_norm)
            optimizer.step()
            num_iter += 1
        valid_loss = 0
        valid_acc = 0
        num_iter = 0
        model.eval()
        with torch.no_grad():
            for batch in valid_loader:
                input_embeds = batch["inputs"].to(device)
                labels = batch["labels"].to(device)
                prediction = model(input_embeds)
                valid_loss += criterion(prediction, labels)
                valid_acc += (labels == torch.max(prediction, axis=1)[1]).float().mean()
                num_iter += 1
        logger.info(f"Epoch: {e+1}/{num_epochs}: valid Loss = {valid_loss/num_iter}, accuracy = {valid_acc/num_iter}")
