import torch
import numpy as np
from utils.logger_supports import stdout_handler

import logging
logger = logging.Logger('collator', level=logging.DEBUG)
logger.addHandler(stdout_handler())


class CollatorCustomRuWiki:
    def __init__(self, emb_size, pad_token=0):
        self.emb_size = emb_size
        self.pad_token = pad_token

    def __call__(self, batch):
        max_len = max(len(row["inputs"]) for row in batch)
        # выравниевание до максимальной длинны
        input_embeds = np.zeros((len(batch), max_len, self.emb_size))
        # input_embeds.fill(self.pad_token)
        labels = np.zeros((len(batch),))
        for idx, row in enumerate(batch):
            if len(row["inputs"]) == 0:
                logger.error(f'Text is empty for element with ind = {idx} and label = {row["label"]}')
            shape = row["inputs"].shape
            input_embeds[idx][:shape[0], :shape[1]] += row["inputs"]
            # input_embeds[idx][:len(row["inputs"])] += row["inputs"]
            labels[idx] = row["label"]
        return {"inputs": torch.FloatTensor(input_embeds), "labels": torch.LongTensor(labels)}


class CollatorCustomRuWikiPaper:
    def __init__(self, emb_size, pad_token=0):
        self.emb_size = emb_size
        self.pad_token = pad_token

    def __call__(self, batch):
        # тут должен быть одна порция
        assert len(batch) == 1
        labels = batch[0]['label']
        inputs = batch[0]['inputs']
        # logger.info(f'In batch inputs shape = {len(inputs)}, labels shape = {len(labels)}')
        max_len = max(len(item) for item in inputs)
        # выравниевание до максимальной длинны
        input_embeds = np.zeros((len(inputs), max_len, self.emb_size))
        # input_embeds.fill(self.pad_token)
        # labels = np.zeros((len(inputs),))
        for idx, row in enumerate(inputs):
            if len(row) == 0:
                logger.error(f'Text is empty for element with ind = {idx} and label = {row["label"]}')
            shape = row.shape
            input_embeds[idx][:shape[0], :shape[1]] = row
            # input_embeds[idx][:len(row["inputs"])] += row["inputs"]
        # logger.info(f'Out inputs shape:{input_embeds.shape}')
        return {"inputs": torch.FloatTensor(input_embeds), "labels": torch.LongTensor(labels)}

# for embeddings learning
# def collate_fn_v2(batch):
#     max_len = max(len(row["inputs"]) for row in batch)
#     input_embeds = np.zeros((len(batch), max_len))
#     labels = np.zeros((len(batch),))
#     for idx, row in enumerate(batch):
#         input_embeds[idx][:len(row["inputs"])] += row["inputs"]
#         labels[idx] = row["label"]
#     return {"inputs": torch.LongTensor(input_embeds), "labels": torch.LongTensor(labels)}
