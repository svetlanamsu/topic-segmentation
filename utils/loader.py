from torch.utils.data import Dataset
import nltk
import ast

import numpy as np
# from pymystem3 import Mystem
import pymorphy2
from pymorphy2.tokenizers import simple_word_tokenize
import re

from torch.utils.data import Sampler


class AgNewsDataset(Dataset):
    def __init__(self, word2vec, dataset, train=True, max_length=128):
        self.data = dataset["train"] if train else dataset["test"]
        self.tokenizer = nltk.WordPunctTokenizer()
        self.word2vec = word2vec
        self.max_length = max_length  # масимальная длина текстов
        self.mean = np.mean(word2vec.vectors, axis=0)
        self.std = np.std(word2vec.vectors, axis=0)

    def __getitem__(self, item):
        text = self.data[item]["text"]
        tokens = self.tokenizer.tokenize(text.lower())  # слова из текста к начальным формам -> list('word1', 'word2')
        embeds = [
            self.word2vec.get_vector(token)  # построение вектора вложения
            for token in tokens if token in self.word2vec
        ][:self.max_length]
        return {"inputs": (np.array(embeds) - self.mean) / self.std, "label": self.data[item]["label"]}

    def __len__(self):
        return len(self.data)


#TODO: узнать назначение
class TextSampler(Sampler):
    def __init__(self, sampler, batch_size_tokens=1e5):
        self.sampler = sampler
        self.batch_size_tokens = batch_size_tokens

    def __iter__(self):
        batch = []
        max_len = 0
        for ix in self.sampler:
            row = self.sampler.data_source[ix]
            max_len = max(max_len, len(row["inputs"]))
            if (len(batch) + 1) * max_len > self.batch_size_tokens:
                yield batch
                batch = []
                max_len = len(row["inputs"])
            batch.append(ix)
        if len(batch) > 0:
            yield batch


class AgNewsDatasetv2(Dataset):
    """
    для моделей с дообучением эмбеддингов
    """
    def __init__(self, dataset, train=True, max_length=128):
        self.data = dataset["train"] if train else dataset["test"]
        self.tokenizer = nltk.WordPunctTokenizer()
        self.max_length = max_length
        # составление словаря: склеить все тексты токенизировать и взять неповторяющиеся
        self.vocab = set(
            self.tokenizer.tokenize("".join(d["text"].lower() for d in self.data))
        )
        # соответствие слова из словаря и его индеска 0 - неизвестное слово
        self.word2idx = {word: idx + 1 for idx, word in enumerate(self.vocab)}

    def __getitem__(self, item):
        text = self.data[item]["text"]
        tokens = self.tokenizer.tokenize(text.lower())
        embeds = [self.word2idx.get(token, 0) for token in tokens][:self.max_length]
        return {"inputs": embeds, "label": self.data[item]["label"]}

    def __len__(self):
        return len(self.data)


# русскаязычные
# все тексты склеяны и подаются подряд
class RuWikiDataset(Dataset):
    def __init__(self, texts, labels, navec, max_length=128):
        self.texts = texts
        self.labels = labels
        # self.tokenizer = Mystem()
        self.tokenizer = pymorphy2.MorphAnalyzer()
        self.navec = navec
        self.max_length = max_length  # масимальная длина текстов
        # self.mean = np.mean(self.navec.as_gensim.vectors, axis=0)
        # self.std = np.std(self.navec.as_gensim.vectors, axis=0)

    def __getitem__(self, item):
        text = self.texts[item]
        # only text
        pure_text = re.sub(r'[^А-Яа-яЁё\s]', ' ', text)
        # pymorphy
        tokens = [self.tokenizer.parse(token)[0].normal_form for token in simple_word_tokenize(pure_text.lower())]
        # Mystem
        # tokens = [lem for lem in self.tokenizer.lemmatize(pure_text) if lem.strip()]
        embeds = [
            self.navec[token]
            for token in tokens if token in self.navec
        ][:self.max_length]
        if len(embeds) == 0:
            # временная заглушка, на случай если нет ни одного найденного погружения
            embeds.append(self.navec['<unk>'])
        return {"inputs": np.array(embeds), "label": self.labels[item]}

    def __len__(self):
        return len(self.texts)


# один элемент - одна статья
class RuWikiPaperDataset(Dataset):
    def __init__(self, dataframe, navec, max_length=128):
        self.texts = dataframe['sentences']
        self.labels = dataframe['labels']
        self.tokenizer = pymorphy2.MorphAnalyzer()
        self.navec = navec
        self.max_length = max_length  # масимальная длина текстов

    def __getitem__(self, item):
        list_sentences = ast.literal_eval(self.texts.iloc[item])
        inputs = list()  # массив вложений
        for sentence in list_sentences:
            pure_text = re.sub(r'[^А-Яа-яЁё\s]', ' ', sentence).strip()
            # TODO: нужна ли обработка на случай если одно слово в предложении или вообще пустое???
            # pymorphy
            tokens = [self.tokenizer.parse(token)[0].normal_form for token in simple_word_tokenize(pure_text.lower())]
            embeds = [
                self.navec[token]
                for token in tokens if token in self.navec
            ][:self.max_length]
            if len(embeds) == 0:
                # временная заглушка, на случай если нет ни одного найденного погружения
                embeds.append(self.navec['<unk>'])
            inputs.append(np.array(embeds))
        labels = np.fromstring(self.labels.iloc[item][1:-1], dtype=int, sep='. ')
        if len(list_sentences) != labels.shape[0]:
            labels = np.fromstring(self.labels.iloc[item][1:-1], dtype=int, sep=',')
        return {"inputs": inputs, "label": labels}

    def __len__(self):
        return len(self.texts)


class RuWikiDatasetEmb(Dataset):
    """
    для моделей с дообучением эмбеддингов
    """
    def __init__(self, texts, labels, navec, max_length=128):
        self.texts = texts
        self.labels = labels
        # self.tokenizer = Mystem()
        self.tokenizer = pymorphy2.MorphAnalyzer()
        self.navec = navec
        self.max_length = max_length
        # составление словаря: склеить все тексты токенизировать и взять неповторяющиеся
        self.vocab = self.navec.vocab

    def __getitem__(self, item):
        text = self.texts[item]
        # only text
        pure_text = re.sub(r'[^А-Яа-яЁё\s]', ' ', text)
        tokens = [self.tokenizer.parse(token)[0].normal_form for token in simple_word_tokenize(pure_text.lower())]
        embeds = [self.vocab.get(token, self.vocab['<unk>']) for token in tokens][:self.max_length]
        return {"inputs": embeds, "label": self.labels[item]}

    def __len__(self):
        return len(self.texts)


if __name__ == '__main__':
    import pandas as pd
    from sklearn.model_selection import train_test_split
    import ast

    dataset = pd.read_csv('../data/ruwiki_data_1000.csv')
    train_data, test_data = train_test_split(dataset, test_size=.2, random_state=100500)

    def create_dataset(data):
        corpus = list()
        labels = list()  # np.empty(shape=0)
        for _, item in data[['sentences', 'labels']].iterrows():
            lls = np.fromstring(item['labels'][1:-1], dtype=int, sep='. ')
            cs = ast.literal_eval(item['sentences'])
            for j in range(lls.shape[0]):
                clear_text = re.sub(r'[^А-Яа-яЁё\s]', r' ', cs[j]).strip()
                if clear_text and len(clear_text.split()) > 1:
                    corpus.append(cs[j])
                    labels.append(lls[j])
        return corpus, labels

    train_corpus, train_labels = create_dataset(train_data)
    test_corpus, test_labels = create_dataset(test_data)
    train_dataset = RuWikiDataset(texts=train_corpus, labels=train_labels)
    for _ in range(2):
        for i in range(len(train_corpus)):
            train_dataset[i]
            print('>>>>>>', i)
