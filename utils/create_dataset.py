import numpy as np
import ast
import re
from torch.utils.data import DataLoader

from utils.logger_supports import stdout_handler

import logging
logger = logging.Logger('create dataset', level=logging.DEBUG)
logger.addHandler(stdout_handler())


def create_dataloader(data, DatasetClass, Sampler, TextSampler, navec, collate_fn=None, num_workers=4):
    logger.info('Start process of dataloder creation from DataFrame.')
    corpus = list()
    labels = list()
    for _, item in data[['sentences', 'labels']].iterrows():
        lls = np.fromstring(item['labels'][1:-1], dtype=int, sep='. ')
        cs = ast.literal_eval(item['sentences'])
        for j in range(lls.shape[0]):
            clear_text = re.sub(r'[^А-Яа-яЁё\s]', r' ', cs[j]).strip()
            if clear_text and len(clear_text.split()) > 1:
                corpus.append(cs[j])
                labels.append(lls[j])

    dataset = DatasetClass(texts=corpus, labels=labels, navec=navec)
    sampler = Sampler(dataset)
    test_loader = DataLoader(dataset, batch_sampler=TextSampler(sampler), collate_fn=collate_fn,
                             num_workers=num_workers)
    logger.info('Dataloader create successfully.')
    return test_loader


def create_dataloader_paper(data, DatasetClass, Sampler, TextSampler, navec, collate_fn=None, num_workers=2):
    logger.info('Start process of paper dataloder creation from DataFrame.')
    dataset = DatasetClass(dataframe=data, navec=navec)
    loader = DataLoader(dataset, collate_fn=collate_fn, num_workers=num_workers,  batch_size=1)
    logger.info('Dataloader create successfully.')
    return loader
