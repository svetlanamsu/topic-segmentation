from utils.loader import RuWikiPaperDataset
import torch
import torch.nn as nn

from torch.utils.data import RandomSampler
from navec import Navec
from HierarchicalLSTMModel import HierarchicalLSTM
from utils.create_dataset import create_dataloader_paper
from utils.collators import CollatorCustomRuWikiPaper
from utils.check_model import train_and_test


test_ds_file_name = '../data/ruwiki_data_test_746.csv'
ds_file_name = '../data/ruwiki_data_train_9k.csv'
path_navec = '../data/navec_hudlit_v1_12B_500K_300d_100q.tar'
navec = Navec.load(path_navec)
embed_size = navec.as_gensim.vector_size

device = "cuda" if torch.cuda.is_available() else "cpu"
num_classes = 2
model = HierarchicalLSTM(lstm_dim=256,
                         emb_size=embed_size,
                         score_dim=256,
                         bidir=True,
                         num_layers=2,
                         drop_prob=0.20,
                         method='avg').to(device)
# model = torch.load('../data/models/hlstm__ep_50')
# model.to(device=device)

criterion = nn.CrossEntropyLoss()
num_epochs = 20

train_and_test(
    model=model,
    device=device,
    test_ds_file_name=test_ds_file_name,
    ds_file_name=ds_file_name,
    navec=navec,
    CollatorClass=CollatorCustomRuWikiPaper,
    dataloader_creator=create_dataloader_paper,
    DatasetClass=RuWikiPaperDataset,
    sampler=RandomSampler,
    criterion=criterion,
    num_epochs=num_epochs,
    max_grad_norm=1.0,
    saving=True,
    file_name_for_saving='hlstm_avg'
)
