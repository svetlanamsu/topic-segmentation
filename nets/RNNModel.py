import torch.nn as nn
import torch

#
# class RNN(nn.Module):
#     def __init__(self, embed_size, hidden_size):
#         super().__init__()
#
#         self.embed_size = embed_size
#         self.hidden_size = hidden_size
#
#         self.w_h = nn.Parameter(torch.rand(hidden_size, hidden_size))
#         self.b_h = nn.Parameter(torch.rand((1, hidden_size)))
#         self.w_x = nn.Parameter(torch.rand(embed_size, hidden_size))
#         self.b_x = nn.Parameter(torch.rand(1, hidden_size))
#
#     def forward(self, x, hidden=None):
#         '''
#         x – torch.FloatTensor with the shape (bs, *, emb_size)
#         hidden - torch.FloatTensro with the shape (bs, hidden_size)
#         return: torch.FloatTensor with the shape (bs, hidden_size)
#         '''
#         hidden = torch.zeros((x.size(0), self.hidden_size)).to(x.device) if hidden is None else hidden
#         for idx in range(x.size(1)):
#             hidden = torch.tanh(x[:, idx] @ self.w_x + self.b_x + hidden @ self.w_h + self.b_h)
#         return hidden


class RNNModel(nn.Module):
    def __init__(self, embed_size, hidden_size, num_classes=2):
        super().__init__()

        self.rnn = nn.RNN(input_size=embed_size, hidden_size=hidden_size)
        self.cls = nn.Linear(hidden_size, num_classes)

    def forward(self, x):
        x = x.permute(1, 0, 2)
        _, hidden = self.rnn(x)
        output = self.cls(hidden.squeeze(0))
        return output