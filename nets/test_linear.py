from utils.loader import RuWikiDataset, TextSampler
import torch
import torch.nn as nn
from torch.utils.data import SequentialSampler, RandomSampler
from navec import Navec
from LinearModel import LinearModel
from utils.create_dataset import create_dataloader
from utils.collators import CollatorCustomRuWiki
from utils.check_model import train_and_test
from utils.logger_supports import stdout_handler

import logging
logger = logging.Logger('test_linear', level=logging.DEBUG)
logger.addHandler(stdout_handler())

test_ds_file_name = '../data/ruwiki_data_1000.csv'
ds_file_name = '../data/ruwiki_data_11252.csv'
path_navec = '../data/navec_hudlit_v1_12B_500K_300d_100q.tar'
navec = Navec.load(path_navec)
embed_size = navec.as_gensim.vector_size
device = "cuda" if torch.cuda.is_available() else "cpu"
num_classes = 2
model = LinearModel(embed_size, num_classes).to(device)

criterion = nn.CrossEntropyLoss()
num_epochs = 2

train_and_test(
    model=model,
    device=device,
    test_ds_file_name=test_ds_file_name,
    ds_file_name=ds_file_name,
    navec=navec,
    CollatorClass=CollatorCustomRuWiki,
    dataloader_creator=create_dataloader,
    DatasetClass=RuWikiDataset,
    sampler=SequentialSampler,
    criterion=criterion,
    num_epochs=num_epochs,
    max_grad_norm=None,
    file_name_for_saving='linear_cont',
    TextSampler=TextSampler
)
