import torch.nn as nn
import torch

class GRU(nn.Module):
    def __init__(self, embed_size, hidden_size):
        super().__init__()

        self.embed_size = embed_size
        self.hidden_size = hidden_size

        self.w_rh = nn.Parameter(torch.rand(hidden_size, hidden_size))
        self.b_rh = nn.Parameter(torch.rand((1, hidden_size)))
        self.w_rx = nn.Parameter(torch.rand(embed_size, hidden_size))
        self.b_rx = nn.Parameter(torch.rand(1, hidden_size))

        self.w_zh = nn.Parameter(torch.rand(hidden_size, hidden_size))
        self.b_zh = nn.Parameter(torch.rand((1, hidden_size)))
        self.w_zx = nn.Parameter(torch.rand(embed_size, hidden_size))
        self.b_zx = nn.Parameter(torch.rand(1, hidden_size))

        self.w_nh = nn.Parameter(torch.rand(hidden_size, hidden_size))
        self.b_nh = nn.Parameter(torch.rand((1, hidden_size)))
        self.w_nx = nn.Parameter(torch.rand(embed_size, hidden_size))
        self.b_nx = nn.Parameter(torch.rand(1, hidden_size))

    def forward(self, x, hidden=None):
        '''
        x – torch.FloatTensor with the shape (bs, *, emb_size)
        hidden - torch.FloatTensro with the shape (bs, hidden_size)
        return: torch.FloatTensor with the shape (bs, hidden_size)
        '''
        hidden = torch.zeros((x.size(0), self.hidden_size)).to(x.device) if hidden is None else hidden
        for idx in range(x.size(1)):
            r = torch.sigmoid(x[:, idx] @ self.w_rx + self.b_rx + hidden @ self.w_rh + self.b_rh)
            z = torch.sigmoid(x[:, idx] @ self.w_zx + self.b_zx + hidden @ self.w_zh + self.b_zh)
            n = torch.tanh(x[:, idx] @ self.w_zx + self.b_zx + r * (hidden @ self.w_zh + self.b_zh))
            hidden = (1 - z) * n + z * hidden
        return hidden


class GRUModel(nn.Module):
    def __init__(self, embed_size, hidden_size, num_classes=2):
        super().__init__()

        self.gru = GRU(embed_size, hidden_size)
        self.cls = nn.Linear(hidden_size, num_classes)

    def forward(self, x):
        hidden = self.gru(x)
        output = self.cls(hidden)
        return output


class GRUModelEmb(nn.Module):
    def __init__(self, voc_size, embed_size, hidden_size, num_classes=2):
        super().__init__()

        self.emb = nn.Embedding(voc_size, embed_size)
        self.gru = GRU(embed_size, hidden_size)
        self.cls = nn.Linear(hidden_size, num_classes)

    def forward(self, x):
        x = self.emb(x)
        hidden = self.gru(x)
        output = self.cls(hidden)
        return output