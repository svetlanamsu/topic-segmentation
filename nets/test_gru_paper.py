from utils.loader import RuWikiPaperDataset
import torch
import torch.nn as nn

from torch.utils.data import SequentialSampler
from navec import Navec
from GRUModel import GRUModel
from utils.create_dataset import create_dataloader_paper
from utils.collators import CollatorCustomRuWikiPaper
from utils.check_model import train_and_test


test_ds_file_name = '../data/test_dataset.csv'
ds_file_name = '../data/ruwiki_data_11252.csv'
path_navec = '../data/navec_hudlit_v1_12B_500K_300d_100q.tar'
navec = Navec.load(path_navec)
embed_size = navec.as_gensim.vector_size

device = "cuda" if torch.cuda.is_available() else "cpu"
num_classes = 2
model = GRUModel(embed_size, num_classes).to(device)
# model = torch.load('../data/models/gru_test_paper_ep_5_ep_5')
# model.to(device=device)
criterion = nn.CrossEntropyLoss()
num_epochs = 10

train_and_test(
    model=model,
    device=device,
    test_ds_file_name=test_ds_file_name,
    ds_file_name=test_ds_file_name,
    navec=navec,
    CollatorClass=CollatorCustomRuWikiPaper,
    dataloader_creator=create_dataloader_paper,
    DatasetClass=RuWikiPaperDataset,
    sampler=SequentialSampler,
    criterion=criterion,
    num_epochs=num_epochs,
    max_grad_norm=1.0,
    saving=True,
    file_name_for_saving='gru_test_paper_test_adadelta'
)
