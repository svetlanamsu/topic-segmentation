from utils.loader import RuWikiPaperDataset
import torch
import torch.nn as nn

from torch.utils.data import SequentialSampler
from navec import Navec
from RNNModel import RNNModel
from utils.create_dataset import create_dataloader_paper
from utils.collators import CollatorCustomRuWikiPaper
from utils.check_model import train_and_test


test_ds_file_name = '../data/ruwiki_data_1000.csv'
ds_file_name = '../data/ruwiki_data_11252.csv'
path_navec = '../data/navec_hudlit_v1_12B_500K_300d_100q.tar'
navec = Navec.load(path_navec)
embed_size = navec.as_gensim.vector_size

device = "cuda" if torch.cuda.is_available() else "cpu"
num_classes = 2
#model = RNNModel(embed_size, num_classes).to(device)
model = torch.load('../data/models/rnn_test_paper_ep_10')
model.to(device=device)
criterion = nn.CrossEntropyLoss()
num_epochs = 30

train_and_test(
    model=model,
    device=device,
    test_ds_file_name=test_ds_file_name,
    ds_file_name=ds_file_name,
    navec=navec,
    CollatorClass=CollatorCustomRuWikiPaper,
    dataloader_creator=create_dataloader_paper,
    DatasetClass=RuWikiPaperDataset,
    sampler=SequentialSampler,
    criterion=criterion,
    num_epochs=num_epochs,
    max_grad_norm=1.0,
    saving=True,
    file_name_for_saving='rnn_test_paper_ep_10'
)
