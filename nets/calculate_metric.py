import pandas as pd
import torch
import numpy as np
from torch.functional import F
from utils.loader import RuWikiPaperDataset, TextSampler
from torch.utils.data import SequentialSampler
from navec import Navec
from utils.create_dataset import create_dataloader_paper
from utils.collators import CollatorCustomRuWikiPaper
from sklearn.metrics import precision_recall_fscore_support

from nltk.metrics.segmentation import pk
from nltk.metrics.segmentation import windowdiff
from nltk.metrics.segmentation import ghd

from utils.logger_supports import stdout_handler
import logging
logger = logging.Logger('calculate metrics', level=logging.DEBUG)
logger.addHandler(stdout_handler())

data = pd.read_csv('../data/test_dataset.csv')


def get_metrics(predicted, referense):
    precision_list = []
    recall_list = []
    f1_list = []

    pk_score = []
    wd_score = []
    ghd_score = []

    for i in range(len(predicted)):
        logger.info(f'iteration {i}/{len(predicted)}')
        precision, recall, f1, _ = precision_recall_fscore_support(
            predicted[i][:-1],
            referense[i][:-1],
            average='binary',
            zero_division=0
        )
        precision_list.append(precision)
        recall_list.append(recall)
        f1_list.append(f1)

        boundaries_ref = ''.join(list(map(str, referense[i])))[:-1]
        boundaries_pred = ''.join(list(map(str, predicted[i])))[:-1]

        if boundaries_ref.count('1'):
            k = int(round(len(boundaries_ref) / (boundaries_ref.count('1') * 2.)))
        else:
            k = 1

        pk_score.append(pk(boundaries_ref, boundaries_pred, k))
        wd_score.append(windowdiff(boundaries_ref, boundaries_pred, k))
        ghd_score.append(ghd(boundaries_ref, boundaries_pred, k))

    precision = round(np.mean(precision_list), 2)
    recall = round(np.mean(recall_list), 2)
    f1 = round(np.mean(f1_list), 2)

    pk_score = round(np.mean(pk_score), 2)
    wd_score = round(np.mean(wd_score), 2)
    ghd_score = round(np.mean(ghd_score), 2)

    return precision, recall, f1, pk_score, wd_score, ghd_score


path_navec = '../data/navec_hudlit_v1_12B_500K_300d_100q.tar'
navec = Navec.load(path_navec)
embed_size = navec.as_gensim.vector_size
my_collator = CollatorCustomRuWikiPaper(emb_size=embed_size, pad_token=navec.vocab['<pad>'])

test_loader = create_dataloader_paper(data=data, DatasetClass=RuWikiPaperDataset, Sampler=SequentialSampler,
                                 TextSampler=TextSampler, navec=navec, collate_fn=my_collator)

device = "cuda" if torch.cuda.is_available() else "cpu"

model = torch.load('../data/models/linear_paper_ep_2')
model.to(device=device)
model.eval()

logger.info(f'Start calculate metrics for model: {model}.')
predictions = list()
true_labels = list()
for batch in test_loader:
    input_embeds = batch["inputs"].to(device)
    labels = batch["labels"]
    prediction = F.softmax(model(input_embeds), dim=1).argmax(dim=1).to('cpu')
    true_labels.append(labels.numpy())
    predictions.append(prediction.numpy())

metrics = get_metrics(predictions, true_labels)
logger.info(f'metrics : {metrics}')
