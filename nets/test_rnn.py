from utils.loader import RuWikiDataset, TextSampler
import torch
import torch.nn as nn
import pandas as pd

from sklearn.model_selection import train_test_split

from torch.utils.data import SequentialSampler, RandomSampler
from navec import Navec
from RNNModel import RNNModel
from utils.trainer import training
from utils.create_dataset import create_dataloader
from utils.saver import save_results
from utils.collators import CollatorCustomRuWiki
from utils.logger_supports import stdout_handler

import logging
logger = logging.Logger('test_rnn', level=logging.DEBUG)
logger.addHandler(stdout_handler())

torch.random.manual_seed(42)
torch.cuda.random.manual_seed_all(42)


test_ds_file_name = '../data/ruwiki_data_1000.csv'
logger.info(f'Load dataset from {test_ds_file_name}.')
test_dataset = pd.read_csv(test_ds_file_name)

ds_file_name = '../data/ruwiki_data_11252.csv'
logger.info(f'Load dataset from {ds_file_name}.')
dataset = pd.read_csv(ds_file_name)

train_data, valid_data = train_test_split(dataset, test_size=.2, random_state=100500)

path_navec = '../data/navec_hudlit_v1_12B_500K_300d_100q.tar'
navec = Navec.load(path_navec)
embed_size = navec.as_gensim.vector_size

my_collator = CollatorCustomRuWiki(emb_size=embed_size, pad_token=navec.vocab['<pad>'])

train_loader = create_dataloader(data=train_data, DatasetClass=RuWikiDataset, Sampler=SequentialSampler,
                                 TextSampler=TextSampler, navec=navec, collate_fn=my_collator)
valid_loader = create_dataloader(data=valid_data, DatasetClass=RuWikiDataset, Sampler=SequentialSampler,
                                 TextSampler=TextSampler, navec=navec, collate_fn=my_collator)


device = "cuda" if torch.cuda.is_available() else "cpu"
num_classes = 2
model = RNNModel(embed_size, 50, num_classes).to(device)

criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=1e-2)
num_epochs = 2

training(model=model,
         criterion=criterion,
         optimizer=optimizer,
         num_epochs=num_epochs,
         train_loader=train_loader,
         valid_loader=valid_loader,
         device=device,
         max_grad_norm=1.0
         )

# save test results in file
test_loader = create_dataloader(data=test_dataset, DatasetClass=RuWikiDataset, Sampler=SequentialSampler,
                                TextSampler=TextSampler, navec=navec, collate_fn=my_collator)
save_results(model=model, data_loader=test_loader, device=device, file_name=f'rnn_test_ep_{num_epochs}')
